# Simple Træfik

This is a simple reverse proxy configuration for your Docker

## Configuration

Copy the .env-dist file as .env for define your own configuration:

`mv .env-dist .env`

And edit it!

```dotenv
# Version of Træfik to use. We prefer fix it to prevent errors due to new version major changes.
TRAEFIK_IMAGE_TAG=maroilles
# Port to use in the webbrowser to reach the Træfik Dashboard.
# Here: http://localhost:8080/dashboard
TRAEFIK_DASHBOARD_PORT=8080
# URL to use in the webbrowser to reach the Træfik Dashboard (in HTTP, without :port)
# Here: http://traefik.localhost/dashboard
# You can set several domains, separating them by a coma, e.g.: traefik.localhost,mylittlebeaver.localhost
TRAEFIK_DOMAIN=traefik.localhost
```

## Usage

### Træfik

Two simple commands:

 * `docker-compose up -d` for run it
 * `docker-compose stop` for stop it

When traefik is run, you can access its webUI, by using the link you defined in the .env TRAEFIK_DOMAIN variable.

### In your apps

Just add the following lines in the `docker-compose.yml` file of your app:

```yml
version: 3

services:
  my-service:
    image: image-name:version
    labels:
      - "traefik.frontend.rule=Host:myapp.docker.localhost"
```
